﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AMS2.Models;
using AMS2.Models.Interface;

namespace AMS2.Models
{
    public class AssetMainRepo:IMainRepo
    {
        AMS2.Models.AMSEntities db;

        public void Create(AMS_Main instance)
        {
            throw new NotImplementedException();
        }

        public void Update(AMS_Main instance)
        {
            throw new NotImplementedException();
        }

        public string Edit(AMS_Main vm)
        {
            IEnumerable<AMS_Main> Maines;
            AMS_Main d;

            using (db = new Models.AMSEntities())
            {
                Maines = db.AMS_Main.Where(x => x.AssetID == vm.AssetID).ToList();
                if (Maines.Count() == 0)
                {
                    //d = new Main();
                    //int maxid = db.Mains.Max(x => x.ID) + 1;
                    //d.ID = maxid;
                    //d.Code = "";
                    //d.Name = vm.Name;
                    //d.Cost = vm.Cost;
                    //d.Discount = vm.Discount;
                    //d.Hanger = vm.Hanger;
                    //d.PerchaseDate = vm.PerchaseDate;
                    //d.RentPrice = vm.RentPrice;
                    //d.Typ = vm.Typ;
                    //d.Color = vm.Color;
                    //d.VenderID = vm.VenderID;
                    //d.InsertDate = DateTime.Now;
                    //db.Maines.Add(d);
                    //db.SaveChanges();
                    return "Can not insert now!!";
                }
                else
                {
                    d = Maines.Where(x => x.AssetID == vm.AssetID).SingleOrDefault();
                    d.AssetName = vm.AssetName;
                    d.EmpName = vm.EmpName;
                    d.InventoryNo = vm.InventoryNo;
                    d.LocationID = vm.LocationID;
                    d.OwnerEmpID = vm.OwnerEmpID;
                    d.OwnerGroupID = vm.OwnerGroupID;
                    d.StatusID = vm.StatusID;
                    d.IsChecked = vm.IsChecked;
                    d.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                    return vm.AssetID.ToString();
                }
            }
        }

        public void Delete(AMS_Main instance)
        {
            throw new NotImplementedException();
        }

        public bool IsAddNew(System.Linq.Expressions.Expression<Func<AMS_Main, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public int? MaxSN(System.Linq.Expressions.Expression<Func<AMS_Main, int?>> predicate)
        {
            throw new NotImplementedException();
        }

        public AMS_Main Get(System.Linq.Expressions.Expression<Func<AMS_Main, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AMS_Main> GetAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<AMS_Main> GetAll(System.Linq.Expressions.Expression<Func<AMS_Main, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}