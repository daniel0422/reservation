﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "AssetGrid",
            //    url: "{control}/{action}/{fn}/{sort}",
            //    defaults: new { controller = "Asset", action = "Grid", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Query",
                url: "Asset/MobileQuery/{typ}",
                defaults: new { controller = "Asset", action = "MobileQuery", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Grid",
                url: "Asset/Grid/{typ}",
                defaults: new { controller = "Asset", action = "Grid", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Asset", action = "Grid", id = UrlParameter.Optional }
            );
        }
    }
}