﻿為了使用內部網路範本，您需要啟用 Windows 驗證。
並停用匿名驗證。

如需詳細指示 (包括 IIS 6.0 的指示)，請瀏覽
http://go.microsoft.com/fwlink/?LinkID=213745

IIS 7 與 IIS 8
1. 開啟 IIS 管理員，然後瀏覽至您的網站。
2. 在 [功能檢視] 中，按兩下 [驗證]。
3. 選取 [驗證] 頁面上的 [Windows 驗證]。若使用 Windows
   則沒有驗證的選項，您必須確認 Windows 驗證
   已安裝在伺服器中。

      若要在 Windows 上啟用 Windows 驗證：
      a) 在 [控制台] 中開啟 [程式和功能]。
      b) 選取 [開啟或關閉 Windows 功能]。
      c) 導覽至 Internet Information Services > World Wide Web 服務 > 安全性
         並確定已勾選 Windows 驗證節點。

      若要在 Windows 伺服器上啟用 Windows 驗證：
      a) 在伺服器管理員中，選取 Web Server (IIS)，並按一下 [新增角色服務]。
      b) 導覽至 Web Server > 安全性
         並確定已勾選 Windows 驗證節點。

4. 按一下 [動作] 窗格中的 [啟用]，即可使用 Windows 驗證。
5. 在 [驗證] 頁面上，選取 [匿名驗證]。
6. 在 [動作] 窗格中，按一下 [停用]，便能停用匿名驗證。

IIS Express
1. 以滑鼠右鍵按一下 Visual Studio 中的專案，然後選取 [使用 IIS Express]。
2. 按一下 [方案總管] 中的專案，選取專案。
3. 如果 [屬性] 窗格尚未開啟，請將它開啟 (F4)。
4. 在專案的 [屬性] 窗格中：
 a) 將 [匿名驗證] 設定為 [停用]。
 b) 將 [Windows 驗證] 設定為 [啟用]。

您可以使用 Microsoft Web Platform Installer 安裝 IIS Express：
    若使用 Visual Studio：http://go.microsoft.com/fwlink/?LinkID=214802
    若使用 Visual Web Developer：http://go.microsoft.com/fwlink/?LinkID=214800
