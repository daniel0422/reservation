﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;


namespace CommonLib.Interface {
    public interface IRepo<TEntity>:IDisposable
    where TEntity : class {
        void Create(TEntity instance);  //新增
        void Update(TEntity instance);  //修改
        void Delete(TEntity instance);  //刪除

        /// <summary>
        /// 該筆資料是否已經存在?如果篩選條件存在的話
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool IsAddNew(Expression<Func<TEntity, bool>> predicate);
        int? Max(Expression<Func<TEntity, int?>> predicate);
        
        //撈取單筆資料
        TEntity Get(Expression<Func<TEntity, bool>> predicate);
        
        //無條件全部撈取多筆資料
        IQueryable<TEntity> GetAll();
        
        //無條件撈取多筆資料
        IQueryable<TEntity> GetAllBy(Expression<Func<TEntity, bool>> predicate);
        void SaveChanges();
    }
}