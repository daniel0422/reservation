﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonLib.Interface;
using System.Data.Entity; //有兩種Entity,Objects
using System.Data.Objects;
using Reservation.Models;
using System.Data;
using System.Data.Entity.Validation;


namespace CommonLib.Repository {
    
    public class GenericRepo<TEntity>:IRepo<TEntity> 
        where TEntity:class
    {
        public GenericRepo(DbContext c) {
            if (c == null) {
                throw new ArgumentNullException("context");
            }
            this._context = c;
        }

        /// <summary>
        /// GenericRepo 繼承已接收BookingDB 資料庫的自己
        /// EF 4.0 uses the ObjectContext 
        /// EF 4.1+ uses the DbContext
        /// ObjectContext 轉成 DbContext work around 的做法!
        /// http://stackoverflow.com/questions/11032683/objectcontext-does-not-contain-a-definition-for-entry-and-no-extension-metho
        /// 但，這邊的BookingEntities有相依性
        /// </summary>
        public GenericRepo()
            : this(new DbContext(new BookingEntities(), true)) {
        }

        private DbContext _context { get; set; }


        #region IRepo<TEntity> 成員

        public void Create(TEntity instance) {
            if (instance == null) {
                throw new ArgumentNullException("instance");
            } else {
                this._context.Entry(instance).State = EntityState.Added;
                this._context.Set<TEntity>().Add(instance);
                //this._context.CreateObjectSet<TEntity>().AddObject(instance);
                this.SaveChanges();
            }
        }

        public void Delete(TEntity instance) {
            if (instance == null) {
                throw new ArgumentNullException("instance");
            } else {
                this._context.Entry(instance).State = EntityState.Deleted;
                this.SaveChanges();
            }
        }

        public TEntity Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate) {
            return this._context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public IQueryable<TEntity> GetAll() {
            return this._context.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> GetAllBy(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate) {
            return _context.Set<TEntity>().Where(predicate);
        }

        public bool IsAddNew(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate) {
            var bb = this._context.Set<TEntity>().Count(predicate);
            return (bb > 0) ? false : true;
        }

        public int? Max(System.Linq.Expressions.Expression<Func<TEntity, int?>> predicate) {
            throw new NotImplementedException();
        }

        public void SaveChanges() {
            try {
                this._context.SaveChanges();
            } catch (DbEntityValidationException e) {
                foreach (var eve in e.EntityValidationErrors) {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors) {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }   
        }

        public void Update(TEntity instance) {
            if (instance == null) {
                throw new ArgumentNullException("instance");
            } else {
                this._context.Entry(instance).State = EntityState.Modified;
                this.SaveChanges();
            }
        }
        #endregion

        #region IDisposable 成員

        public void Dispose() {
            throw new NotImplementedException();
        }

        #endregion

    }
}