﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reservation.Models;

namespace Reservation.ViewModels
{
    public class ReservViewModel
    {
        public Reserve Reserves { get; set; }
        public string DefaultRoom { get; set; }
        public IEnumerable<Room> Rooms { get; set; }
        public IEnumerable<Room> RoomDisabled { get; set; }
    }
}