﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AMS.Models;
using System.Collections;
using AMS2.Models;

namespace AMS2.ViewModels
{
    public class AMSViewModel
    {
        public IEnumerable<AMS_Location> Locations { get; set; }
        public IEnumerable<vAMS_Owner> Owner { get; set; }
        public IEnumerable<AMS_OwnerGroup> Groups { get; set; }
        public string OwnerAZOptions { get; set; }

        public AMS_Main AmsMain { get; set; }
        public IEnumerable<AMS_MainLog> AmsMainlogs { get; set; }
        public string ErrorMsg { get; set; }
        public string OddAssetID { get; set; }

        public int CheckCC { get; set; }
        public int TotalCC { get; set; }
        public string Typ { get; set; }
    }

    public class GridViewModel
    {
        public IEnumerable<AMS_Location> Locations { get; set; }
        public IEnumerable<vAMS_Owner> Owner { get; set; }
        public IEnumerable<AMS_OwnerGroup> Groups { get; set; }

        public IEnumerable<AMS_Main> AmsMain { get; set; }
        public IEnumerable<AMS_MainLog> AmsMainlogs { get; set; }
        public string ErrorMsg { get; set; }
        public string OddAssetID { get; set; }

        public int CheckCC { get; set; }
        public int TotalCC { get; set; }
    }
}