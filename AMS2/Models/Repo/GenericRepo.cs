﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Objects;
using System.Data;
using System.Linq.Expressions;
using AMS2.Models;
using AMS2.Models.Interface;

namespace AMS2.Models
{
    public class GenericRepo<TEntity>: IRepo<TEntity>
        where TEntity:class
    {
        private DbContext _context { get; set; }
        
        //這邊預設Frei資料庫!
        public GenericRepo(): this(new AMSEntities())
        {
        
        }

        public GenericRepo(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }            
            this._context = context;
        }
 
        public GenericRepo(ObjectContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this._context = new DbContext(context, true);
        }


         /// <summary>
        /// Creates the specified instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        public void Create(TEntity instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            else
            {
                this._context.Entry(instance).State = EntityState.Added;
                this._context.Set<TEntity>().Add(instance);
                this.SaveChanges();
            }
 
        }

        /// <summary>
        /// Updates the specified instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Update(TEntity instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            else
            {
                this._context.Entry(instance).State = EntityState.Modified;
                this.SaveChanges();
            }
        }

        public string Edit(TEntity instance)
        {
            if (instance == null)
            {
                throw new NotImplementedException();
            }
            else 
            { 
                
            }
            return "ss";
        }

        /// <summary>
        /// Deletes the specified instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Delete(TEntity instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            else
            {
                this._context.Entry(instance).State = EntityState.Deleted;
                this.SaveChanges();
            }
        }

        /// <summary>
        /// Gets the specified predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return this._context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public IQueryable<TEntity> GetAll()
        {
            return this._context.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        public bool IsAddNew(Expression<Func<TEntity, bool>> predicate)
        {
            var bb = this._context.Set<TEntity>().Count(predicate);
            return (bb > 0) ? false : true;
        }

        public int? MaxSN(Expression<Func<TEntity, int?>> predicate)
        {
            var imax = this._context.Set<TEntity>().Max(predicate);
            imax=(imax == null) ? 0 : imax;
            return imax;
        }

        public void SaveChanges()
        {

                this._context.SaveChanges();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._context != null)
                {
                    this._context.Dispose();
                    this._context = null;
                }
            }
        }


      
    }
}